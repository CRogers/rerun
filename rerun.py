import os # used for opening programs in the filesystem
import sys # used for getting command line arguments (if any)
import gi # used for gtk interface
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GdkPixbuf
scriptPath = os.path.dirname(os.path.realpath(sys.argv[0]))

pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
        filename=scriptPath+"/icons/edit_icon_white.svg", 
        width=20, 
        height=20, 
        preserve_aspect_ratio=True)

#read in extensions and associated programs


def run_all(fileName): #takes a file or folder name and runs programs associated with it.
	extDict = {} #this will store file types and associations in a dictionary with the extensions as key values to retrieve the associated programs.
	
	file_object = open(scriptPath+'/config/filetypes.csv', 'r') #load programs/associations file
	lines=file_object.readlines()
	for l in lines: 
		ext = (l[:l.find(':')]).strip() # get extension key value
		programs = (l[l.find(':')+1:]).strip() # get associated programs
		extDict[ext] = programs; #add ext and programs to dictionary
#		print(extDict["html"])

	if fileName[0] != '#':
		if fileName.find('http') > -1:
			os.system(extDict['http']+' '+fileName+" &")
		elif fileName[-1:] == '/':
			os.system(extDict['folder']+' '+fileName+" &")
		elif fileName.rfind('.'):
#			print dot_array
			ext = fileName[fileName.rfind('.')+1:].lower() 
			programs = extDict[ext].split(' ')
#			print("ext:"+ext)
			for p in programs:
				os.system(p+' '+fileName+" &")
		else:
			print ("Sorry, there are no programs associated with "+fileName+". Please contact the developer with the file that caused this error.")
				
def run_button_clicked(widget, fileName):
	file_object = open(scriptPath+'/projects/'+fileName, 'r')
	lines=file_object.readlines()
	for l in lines:
		run_all(l.strip())

def edit_button_clicked(widget, fileName):
	os.system('gedit '+scriptPath+'/projects/'+fileName+' &')

def show_project(widget, gridObject, fileName):
	file_object = open(scriptPath+'/projects/'+fileName, 'r')
	lines=file_object.readlines()
	texboxArray=[]
	x = 0
	for l in lines:
		texboxArray.append(Gtk.Entry.new_with_buffer(Gtk.EntryBuffer.new(l,len(l))))
		gridObject.attach(texboxArray[x],0,x,1,1)
		x+=1;
#		print(x)
	gridObject.show_all()

class ButtonWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="ReRun Ver 0.1 Alpha")
		self.set_border_width(10)
		hgrid = Gtk.Grid()
		Gtk.Orientation.VERTICAL
		self.add(hgrid)
		myFileNames=os.listdir(scriptPath+"/projects/")
		myFileNames.sort()
#		print(myFileNames)
		x=0 ## button_list index counter
		button_list=[]
		edit_button_list=[]

		infoGrid = Gtk.Grid()
 		for file in [f for f in myFileNames if f.endswith('.rerun')]:
			button_list.append(Gtk.Button.new_with_label(file[:file.find('.')]))
			edit_button_list.append(Gtk.Button.new())
			button_list[x].connect("clicked",run_button_clicked,file)
			edit_button_list[x].connect("clicked",edit_button_clicked,file)
			image = Gtk.Image.new_from_pixbuf(pixbuf)
			edit_button_list[x].add(image)
			edit_button_list[x].set_size_request(20,20)
			hgrid.attach(button_list[x],0,x,1,1)
			hgrid.attach(edit_button_list[x],1,x,1,1)
			x += 1

#		infoFrame=Gtk.Frame.new("info")
#		hgrid.attach(infoFrame,2,0,10,len(button_list))
#		infoFrame.set_size_request(400,100)
#		infoFrame.set_label("Woooah!")
#		infoFrame.add(infoGrid)

			
win = ButtonWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()



